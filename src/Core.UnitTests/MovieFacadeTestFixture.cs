﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Castle.DynamicProxy.Generators.Emitters;
using MoviesLibrary;
using NSubstitute;
using Should;
using Xunit;


namespace Chris.McKelt.Test.Core.UnitTests
{
    public class MovieFacadeTestFixture
    {
        private MovieFacade _movieFacade;
        private readonly IExternalMovieDataSource _externalMovieDataSource;
        private IEnumerable<LocalMovieData> _stubs;

        public MovieFacadeTestFixture()
        {
            _externalMovieDataSource = Substitute.For<IExternalMovieDataSource>();
            _stubs = GetStubData();
            InMemoryDataStore.Data = _stubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;
            _externalMovieDataSource.GetAllData().Returns(_stubs.Select(a=>a.Data).ToList());
            _movieFacade = new MovieFacade(_externalMovieDataSource); //TODO IOC/Logging
        }

        [Fact]
        public void Sync_should_request_all_data_if_local_data_older_than_1_day()
        {
            //a
            DateTime lastExtract = DateTime.UtcNow.AddDays(-2);
            _movieFacade = new TestMovieFacade(_externalMovieDataSource, lastExtract);
            InMemoryDataStore.Data = null;

            //a
            _movieFacade.Sync();

            //a
            _externalMovieDataSource.Received().GetAllData();
        }

        [Fact]
        public void Sync_should_not_request_all_data_if_within_24_hours_of_last_extract()
        {
            //a
            _movieFacade = new TestMovieFacade(_externalMovieDataSource, DateTime.UtcNow);
            var lastExtract = _movieFacade.GetDataLastModified();

            //a
            _movieFacade.Sync();

            //a
            _externalMovieDataSource.DidNotReceive().GetAllData();
            _movieFacade.GetDataLastModified().ShouldEqual(lastExtract);
        }

        [Fact]
        public void Sync_should_create_new_items_from_local_cache()
        {
            //a
            var movieStubs = GetStubData(3).ToList();
            movieStubs[0].HasChanged = true;
            movieStubs[0].LastModified = DateTime.UtcNow;
            movieStubs[1].HasChanged = true;
            movieStubs[1].LastModified = DateTime.UtcNow;
            movieStubs[2].HasChanged = true;
            movieStubs[2].LastModified = DateTime.UtcNow;

            movieStubs[1].Data.MovieId = 0; // insert this one

            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow.AddDays(-2);

            //a
            _movieFacade.Sync();

            //a
            _externalMovieDataSource.Received(1).Create(movieStubs[1].Data);
        }

        [Fact]
        public void Sync_should_update_items_from_local_cache()
        {
            //a
            var movieStubs = GetStubData(3).ToList();
            movieStubs[0].HasChanged = true;
            movieStubs[0].LastModified = DateTime.UtcNow;
            movieStubs[1].HasChanged = true;
            movieStubs[1].LastModified = DateTime.UtcNow;
            movieStubs[2].HasChanged = true;
            movieStubs[2].LastModified = DateTime.UtcNow;

            movieStubs[1].Data.MovieId = 0; // insert this one

            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow.AddDays(-2);

            //a
            _movieFacade.Sync();

            //a
            _externalMovieDataSource.Received(1).Update(movieStubs[0].Data);
            _externalMovieDataSource.Received(1).Update(movieStubs[2].Data);
        }


        [Fact]
        public void GetAllData_should_limit_max_results_returned()
        {
            //aa
            Assert.Throws<ArgumentOutOfRangeException>(() => _movieFacade.GetAllData(OrderBy.MovieId,MovieFacade.MaxReturnResultSetSize + 1));

        }

        [Fact]
        public void GetAllData_should_guard_against_invalid_paging()
        {
            //aa
            Assert.Throws<ArgumentOutOfRangeException>(() => _movieFacade.GetAllData(OrderBy.MovieId, 10,6000));

        }

        [Fact]
        public void GetAllData_should_return_requested_results()
        {
            //a
            var results = _movieFacade.GetAllData();
            //a
            results.ShouldNotBeEmpty();
            // check default order
            results.First().Data.Title.ShouldEqual(InMemoryDataStore.Data.First().Data.Title);
        }


        [Fact]
        public void GetAllData_should_return_according_to_order_by()
        {
            //a
            var movieStubs = GetStubData(3).ToList();

            movieStubs[0].Data.Title = "a";
            movieStubs[1].Data.Title = "b";
            movieStubs[2].Data.Title = "c";

            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;

            //a
            var results = _movieFacade.GetAllData(OrderBy.Title).ToList();
            //a
            results.First().Data.Title.ShouldEqual("a");
            results[1].Data.Title.ShouldEqual("b");
            results[2].Data.Title.ShouldEqual("c");
        }


        [Fact]
        public void GetDataById_should_get_item_with_specified_id()
        {
            //a
            var last = _stubs.Last().Data;
            var result = _movieFacade.GetDataById(last.MovieId);

            //a
            result.ShouldNotBeNull();
            result.Data.MovieId.ShouldEqual(last.MovieId);
            result.Data.Classification.ShouldEqual(last.Classification);
            result.Data.Genre.ShouldEqual(last.Genre);
            result.Data.Rating.ShouldEqual(last.Rating);
            result.Data.ShouldEqual(last);
        }

        [Fact]
        public void SaveOrUpdate_should_validate_before_save_or_update()
        {
            //a
            var badData = _stubs.Last();
            badData.Data.Title = string.Empty;
            ArgumentException argumentException = null;

            //a
            try
            {
                _movieFacade.SaveOrUpdate(badData);
            }
            catch (ArgumentException ae)
            {
                argumentException = ae;
            }

            //a
            argumentException.Message.Contains("Title invalid");
        }

        [Fact]
        public void SaveOrUpdate_should_update_item()
        {
            //a
            var last = _stubs.Last();
            const string changeText = "this has been changed";
            last.Data.Title = changeText;

            //a
            _movieFacade.SaveOrUpdate(last);

            //a
            var result = InMemoryDataStore.Data.Last();
            result.Data.Title.ShouldEqual(last.Data.Title);
            result.HasChanged.ShouldBeTrue();
            result.LastModified.ShouldNotEqual(null);
            result.Data.ShouldEqual(last.Data);
        }


        [Fact]
        public void SaveOrUpdate_should_insert_item()
        {
            //a
            int total = InMemoryDataStore.Data.Count();
            var md = new MovieData()
            {
                Cast = {},
                Classification = "R",
                Genre = "Action",
                Rating = 3,
                ReleaseDate = 5,
                Title = "The dog ate my homework part 2"
            };
            var item = new LocalMovieData()
            {
                Data = md
            };

            //a
            _movieFacade.SaveOrUpdate(item);

            //a
            InMemoryDataStore.Data.Count().ShouldEqual(total+1);
           
        }


        [Fact]
        public void Search_should_return_results()
        {
            //a
            var movieStubs = GetStubData(3).ToList();
            const string searchText = "aaa";
            movieStubs[0].Data.Title = searchText;
            movieStubs[1].Data.Classification = searchText;
            movieStubs[2].Data.Title = "c";

            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;

            //a
            var results = _movieFacade.Search(searchText);
            //a
            results.Count.ShouldEqual(2);
            results.First().Data.Title.ShouldEqual(searchText);
        }


        [Fact]
        public void Search_should_return_using_search_for_rating()
        {
            //a
            var movieStubs = GetStubData(3).ToList();
            movieStubs[1].Data.Rating = 555;


            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;

            //a
            var results = _movieFacade.Search<int>("555",(md => md.Data.Rating));
            //a
            results.Count.ShouldEqual(1);
            results.First().Data.Rating.ShouldEqual(555);
        }

        [Fact]
        public void Search_should_return_using_search_for_genre()
        {
            //a
            var movieStubs = GetStubData(3).ToList();
            movieStubs[1].Data.Genre = "horror";

            InMemoryDataStore.Data = movieStubs.AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;

            //a
            var results = _movieFacade.Search<string>("horror", (md => md.Data.Genre));
            //a
            results.Count.ShouldEqual(1);
            results.First().Data.Genre.ShouldEqual("horror");
        }


        // ***************************** helper methods below

        private static IEnumerable<LocalMovieData> GetStubData(int numberToCreate = 80)
        {
            var results = new MovieDataSource().GetAllData().Take(numberToCreate).ToList();  //normally I would create my own stub data or use something like NBuilder to generate test stubs - just thought i would reuse your data for simplicity

            return results.Select(result => new LocalMovieData()
            {
                Data = result,
                HasChanged = false,
                LastModified = null
            });
        }

        /// <summary>
        /// Test specific sub class - override allows different last modified dates to be retrieved for testing purposes
        /// </summary>
        private class TestMovieFacade : MovieFacade
        {
            private readonly DateTime _lastExtract;

            public TestMovieFacade(IExternalMovieDataSource externalMovieDataSource, DateTime lastExtract) : base(externalMovieDataSource)
            {
                _lastExtract = lastExtract;
            }

            public override DateTime? GetDataLastModified()
            {
                return _lastExtract;
            }
        }

    }
}
