﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MoviesLibrary;
using Should;
using Xunit;

namespace Chris.McKelt.Test.Core.UnitTests
{
    public class ExtensionsTestFixture
    {
        [Fact]
        public void GetProperty_returns_property_info()
        {
            //a
            const string title = "aaa";
            var md = new MovieData(){Title = title};

            //a
            var result = md.GetProperty(x => x.Title);

            //a
            result.Name.ShouldEqual("Title");
        }

        [Fact]
        public void GetMemberName_should_stringify_a_members_name()
        {
            //a
            const string title = "aaa";
            var md = new MovieData() { Title = title };

            Expression<Func<MovieData, int>> expression = data => data.Rating;

            //a
            var result = Extensions.GetPropertyInfo(expression);

            //a
            result.Name.ShouldEqual("Rating");
        }
    }
}
