﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesLibrary;

namespace Chris.McKelt.Test.Core
{
    /// <summary>
    /// Local record uses composition to hold a MovieData entity and knowledge of any changes
    /// </summary>
    public class LocalMovieData 
    {
        public MovieData Data { get; set; }

        /// <summary>
        /// False means its a local representation of changes
        /// </summary>
        public bool HasChanged { get; set; } 

        public DateTime? LastModified { get; set; }

        // IsDeleted/Audit/etc
    }
}
