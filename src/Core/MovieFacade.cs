﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Remoting.Lifetime;
using MoviesLibrary;

namespace Chris.McKelt.Test.Core
{
    public class MovieFacade : IMovieFacade
    {
        // set a results max limit to disallow a full data request & thus cause perf issues
        public const int MaxReturnResultSetSize = 20;
        private readonly IExternalMovieDataSource _externalMovieDataSource;

        public MovieFacade(IExternalMovieDataSource externalMovieDataSource)
        {
            _externalMovieDataSource = externalMovieDataSource;
        }

        /// <summary>
        /// Call this on a schedule once every 24 hours to sync with external data source 
        /// e.g. use QuartzNet to schedule a daily NServiceBus job to call this method (alert with SCOM if job not successful/activated)
        /// </summary>
        public void Sync()
        {
            if (!GetDataLastModified().HasValue)
            {
                InMemoryDataStore.Data = _externalMovieDataSource.GetAllData().Cast<LocalMovieData>().AsQueryable();
                InMemoryDataStore.LastExtract = DateTime.UtcNow;
            }

            // dont call if within 24 hours of last sync
            if (InMemoryDataStore.Data != null &&
                InMemoryDataStore.LastExtract.GetValueOrDefault(DateTime.UtcNow) >= DateTime.UtcNow.AddDays(-1)) return;

            // update external cache from local
            if (InMemoryDataStore.Data != null)
            {
                var changed = InMemoryDataStore.Data.Where(a => a.HasChanged);

                //TODO drop these on a message bus
                foreach (var locallyChangedData in changed)
                {
                    if (locallyChangedData.Data.MovieId < 1)
                    {
                        // insert
                        locallyChangedData.Data.MovieId = _externalMovieDataSource.Create(locallyChangedData.Data);
                    }
                    else
                    {
                        //update   
                        _externalMovieDataSource.Update(locallyChangedData.Data);
                    }
                }
            }

            InMemoryDataStore.Data = _externalMovieDataSource.GetAllData().Cast<LocalMovieData>().AsQueryable();
            InMemoryDataStore.LastExtract = DateTime.UtcNow;
        }

        public IEnumerable<LocalMovieData> GetAllData(OrderBy orderBy=OrderBy.MovieId,int pagesize = MaxReturnResultSetSize, int page = 0)
        {
            CheckDataExists();

            if (pagesize > MaxReturnResultSetSize)
                throw new ArgumentOutOfRangeException("pagesize", string.Format("PageSize is larger than max allowed returnable results {0} ", MaxReturnResultSetSize));
            var skip = page*pagesize;

            if (skip > InMemoryDataStore.Data.Count() || skip < 0)
                throw new ArgumentOutOfRangeException("page", string.Format("Page is invalid. Range should be between 0 : {0} ", skip));

            var results = InMemoryDataStore.Data.Skip(skip).Take(pagesize);

            switch (orderBy)
            {
                case OrderBy.MovieId:
                    return results.OrderBy(a => a.Data.MovieId).AsEnumerable();
                case OrderBy.Classification:
                    return results.OrderBy(a => a.Data.Classification).AsEnumerable();
                case OrderBy.Rating:
                    return results.OrderBy(a => a.Data.Rating).AsEnumerable();
                case OrderBy.ReleaseDate:
                    return results.OrderBy(a => a.Data.ReleaseDate).AsEnumerable();
                case OrderBy.Title:
                    return results.OrderBy(a => a.Data.Title).AsEnumerable();
                default:
                    throw new ArgumentException("OrderBy not accounted for: " + orderBy.ToString());
            }
           
        }

        public LocalMovieData GetDataById(int id)
        {
            return (from m in InMemoryDataStore.Data
                where m.Data.MovieId == id
                select m).SingleOrDefault();
        }


        public IList<LocalMovieData> Search(string searchText)
        {
            CheckDataExists();
            return Search<string>(searchText);
        }

        public IList<LocalMovieData> Search<T>(string searchText, Expression<Func<LocalMovieData, T>> selector = null, OrderBy orderBy = OrderBy.MovieId)
        {
            CheckDataExists();
            if (selector == null)
            {
                return (from l in InMemoryDataStore.Data
                        where
                        l.Data.Cast.Contains(searchText)
                        || l.Data.Title.Contains(searchText)
                        || l.Data.Classification.Contains(searchText)
                        || l.Data.Genre.Contains(searchText)
                        || SearchInt(l.Data, searchText, data => data.MovieId)
                        || SearchInt(l.Data, searchText, data => data.Rating)
                        || SearchInt(l.Data, searchText, data => data.ReleaseDate)
                        || l.Data.Cast.Any(cast => cast.Contains(searchText)) //could cast be null?
                        select l).ToList();
            }
            else
            {
                // search by specific property
                var propName = Extensions.GetPropertyInfo<LocalMovieData, T>(selector);
                
                var md = new MovieData();
                // pattern matching would be nice here
                if (propName.Name == md.GetProperty(a => a.Title).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.Title.Contains(searchText)).ToList();  //hmm should bound this result set
                }
                else if (propName.Name == md.GetProperty(a => a.Classification).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.Classification.Contains(searchText)).ToList();  // TODO gotta be a better way to do this 
                }
                else if (propName.Name == md.GetProperty(a => a.Genre).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.Genre.Contains(searchText)).ToList();  
                }
                else if (propName.Name == md.GetProperty(a => a.MovieId).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.MovieId == Int32.Parse(searchText)).ToList();
                }
                else if (propName.Name == md.GetProperty(a => a.Rating).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.Rating == Int32.Parse(searchText)).ToList();
                }
                else if (propName.Name == md.GetProperty(a => a.ReleaseDate).Name)
                {
                    return InMemoryDataStore.Data.Where(a => a.Data.ReleaseDate == Int32.Parse(searchText)).ToList();
                }
                else
                {
                    throw new NotImplementedException(propName.Name);
                }

            }

            return null;
        }


        private bool SearchInt(MovieData data, string searchText, Expression<Func<MovieData, int>> selector)
        {
            if (selector.Name == data.GetProperty(a => a.MovieId).Name) //overkill 
            {
                int result = -1;
                if (!Int32.TryParse(searchText, out result)) return false;
                return data.MovieId == result;
            }
            else if (selector.Name == data.GetProperty(a => a.Rating).Name)
            {
                int result = -1;
                if (!Int32.TryParse(searchText, out result)) return false;
                return data.Rating == result;
            }
            else if (selector.Name == data.GetProperty(a => a.ReleaseDate).Name)
            {
                int result = -1;
                if (!Int32.TryParse(searchText, out result)) return false;
                return data.ReleaseDate == result;
            }
            return false;
        }

        public void SaveOrUpdate(LocalMovieData md)
        {
            CheckDataExists();
            var errors = ValidationErrors(md.Data);
            var enumerable= errors as string[] ?? errors.ToArray();
            if (enumerable.Any())
            {
                throw new ArgumentException(string.Join("\n",enumerable));
            }

            // TODO wrap transaction & lock

            if (md.Data != null)
            {
                md.HasChanged = true;
                md.LastModified = DateTime.UtcNow;

                if (md.Data.MovieId > 0)
                {
                    var arr = InMemoryDataStore.Data.ToArray();
                    // update
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Data.MovieId == md.Data.MovieId)
                        {
                            arr[i] = md;
                            //log/audit
                            break;
                        }
                    }

                    InMemoryDataStore.Data = arr.AsQueryable();
                }
                else
                {
                    // insert   
                    var lst = InMemoryDataStore.Data.ToList();
                    lst.Add(md);
                    InMemoryDataStore.Data = lst.AsQueryable();
                }
            }
            else
            {
                throw new ArgumentException("Data cannot be null");
            }
        }


        private IEnumerable<string> ValidationErrors(MovieData md)
        {
            Func<MovieData, Tuple<bool, string>>[] rules =
            {
                a => new Tuple<bool, string>(string.IsNullOrEmpty(a.Title), "Title invalid"),
                b => new Tuple<bool, string>(string.IsNullOrEmpty(b.Classification), "Classification invalid"),
                c => new Tuple<bool, string>(string.IsNullOrEmpty(c.Genre), "Genre invalid"), 
            };

            return rules.Select(rule => rule(md)).Where(a=>a.Item1).Select(b=>b.Item2);
        }


        private void CheckDataExists()
        {
            // TODO AOP implementation to invoke this on methods with a specific attribute
            if (InMemoryDataStore.Data == null)
                throw new DataException("Sync required");
        }

        /// <summary>
        /// virtual method allows test classes to specify date of extract
        /// </summary>
        /// <returns></returns>
        public virtual DateTime? GetDataLastModified()
        {
            return InMemoryDataStore.LastExtract;
        }
    }

    public enum OrderBy
    {
        MovieId,
        Classification,
        Rating,
        Genre,
        ReleaseDate,
        Title,
    }
}
