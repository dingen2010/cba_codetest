﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Chris.McKelt.Test.Core
{
    public static class Extensions
    {
        public static PropertyInfo GetProperty<TX, TY>(this TX obj, Expression<Func<TX, TY>> selector) 
        {
            return GetPropertyInfo(selector);
        }

        public static PropertyInfo GetPropertyInfo<TX, TY>(Expression<Func<TX, TY>> selector) 
        {
            Expression body = selector;
            if (body is LambdaExpression)
            {
                body = ((LambdaExpression) body).Body;
            }
            switch (body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    return (PropertyInfo) ((MemberExpression) body).Member;
                default:
                    throw new InvalidOperationException(body.NodeType.ToString());
            }
        }
    }
}
