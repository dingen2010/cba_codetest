﻿using System;
using System.Linq;
using MoviesLibrary;

namespace Chris.McKelt.Test.Core
{
    /// <summary>
    /// switch this out for a database/distributed cache 
    /// </summary>
    public static class InMemoryDataStore
    {
        public static IQueryable<LocalMovieData> Data { get; set; } // 
        public static DateTime? LastExtract { get; set; }
    }
}