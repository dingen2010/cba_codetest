using System.Collections.Generic;
using MoviesLibrary;

namespace Chris.McKelt.Test.Core
{
    public interface IExternalMovieDataSource
    {
        int Create(MovieData movie);
        List<MovieData> GetAllData();
        MovieData GetDataById(int id);
        void Update(MovieData movie);
    }
}