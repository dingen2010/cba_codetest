using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Chris.McKelt.Test.Core
{
    public interface IMovieFacade
    {
        /// <summary>
        /// Call this on a schedule once every 24 hours to sync with external data source 
        /// e.g. use QuartzNet to schedule a daily NServiceBus job to call this method (alert with SCOM if job not successful/activated)
        /// </summary>
        void Sync();

        IEnumerable<LocalMovieData> GetAllData(OrderBy orderBy=OrderBy.MovieId,int pagesize = MovieFacade.MaxReturnResultSetSize, int page = 0);
        LocalMovieData GetDataById(int id);
        IList<LocalMovieData> Search(string searchText);
        IList<LocalMovieData> Search<T>(string searchText, Expression<Func<LocalMovieData, T>> selector = null, OrderBy orderBy = OrderBy.MovieId);
        void SaveOrUpdate(LocalMovieData md);

        /// <summary>
        /// virtual method allows test classes to specify date of extract
        /// </summary>
        /// <returns></returns>
        DateTime? GetDataLastModified();
    }
}