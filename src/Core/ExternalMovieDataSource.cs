﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesLibrary;

namespace Chris.McKelt.Test.Core
{
    public class ExternalMovieDataSource : IExternalMovieDataSource
    {
        private readonly MovieDataSource _movieDataSource;

        public ExternalMovieDataSource()
        {
            _movieDataSource = new MovieDataSource();
        }

        public int Create(MovieData movie)
        {
            return _movieDataSource.Create(movie);
        }

        public List<MovieData> GetAllData()
        {
            return _movieDataSource.GetAllData();
        }

        public MovieData GetDataById(int id)
        {
            return _movieDataSource.GetDataById(id);
        }

        public void Update(MovieData movie)
        {
            _movieDataSource.Update(movie);
        }
    }
}
