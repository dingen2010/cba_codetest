---------  Design overview --------------

Design uses a local cache of data and syncs with the master cache every 24 hours
Local cache uses an in-memory store - in a real application this would be substituted for a database/cache e.g. Redis

---------  Assumptions --------------

Service type implementation not demonstrated (ie WCF/MSMQ etc)
For WCF --> Wire up interface IMovieFacade with Castle/Autofac as a WCF Service Host implementation

For an exposed WCF interface optional/default values on method calls would be removed and implemented as simple calls

e.g. to Search by classification

	public IList<LocalMovieData> Search<T>(string searchText, Expression<Func<LocalMovieData, T>> selector = null, OrderBy orderBy = OrderBy.MovieId)

would become

	public IList<LocalMovieData> SearchByClassification(string searchText, OrderBy orderBy)    --> which would map onto the original implementation

---------  Improvements --------------

Ask master service vendor to implement a LastModified data so we only pull back changed data
Logging/Auding/IOC/Security/Pub/Sub framework


Please contact chris@mckelt.com or @chris_mckelt for any questions or enhancement requests.

Thanks